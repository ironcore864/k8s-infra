## EKS IAM Editor Role

```
kubectl apply -f aws-auth.yaml
```

## helm-s3 Plugin

```
helm plugin install https://github.com/hypnoglow/helm-s3.git
helm repo add sei s3://seirobotics-helm-charts-stable/
```

## External DNS

```
kubectl apply -f external-dns.yaml
```

## ALB Ingress

```
kubectl apply -f alb-sa.yaml
kubectl apply -f alb-ingress.yaml
```

### ALB with SSL Redirect

https://kubernetes-sigs.github.io/aws-alb-ingress-controller/guide/tasks/ssl_redirect/

## NLB Ingress

```
helm install nlb-ingress -f nlb-ingress.yaml -n kube-system stable/nginx-ingress
```

## Vault

```
cd vault
helm install vault -f myvalues.yaml .
```

